# Where should I store my files?

When it comes to storage location, files can be separated into (at least) three categories:
1. Application files
2. Support files
3. User generated files

## Application files
Application files are non-Java source files required for an application to run. In a Maven project they usually reside under src/main/resources. Examples include configurations, language files, and resources such as images, video, audio and CSS that are part of the user interface.

## Support files

A support file is needed by the application, but is not required to run. Examples include sample files and temporary or cached files. They should reside in a folder separate from the application files. This location is often dictated by the OS. For example:

- In macOS, they should be stored under `~/Library/Application Support`. Apple´s [File System Programming Guide](https://developer.apple.com/library/archive/documentation/FileManagement/Conceptual/FileSystemProgrammingGuide/MacOSXDirectories/MacOSXDirectories.html#//apple_ref/doc/uid/TP40010672-CH10-SW1) states that:

  *"...all of these items should be put in a subdirectory whose name matches the bundle identifier of the app. For example, if your app is named MyApp and has the bundle identifier `com.example.MyApp`, you would put your app’s user-specific data files and resources in the `~/Library/Application Support/com.example.MyApp/` directory. Your app is responsible for creating this directory as needed."*

- Windows uses similar conventions, giving `~\AppData` as the preferred location.

Note that there are also preferred locations for log files, e.g. `~/Library/Logs` in macOS.

Dealing with custom locations for different OSes can be a chore. A simple and accepted alternative is to store such files in a dedicated folder at the root-level of the project.

## User generated files

User generated files are created by the user and are not part of the application. Examples include documents and save files. These files should be stored in a place that is easily accessible to the user. The location is usually picked via a file chooser or save dialog.