# file-handling

Code examples demonstrating file-handling in Java:

- `TextFiles` has generic methods for writing and reading text content

- `BinaryFiles` has generic methods showing how to copy binary data

- Serialisation: demonstrates serialisation and deserialisation of objects

- Pixel: 

  - A `PixelFileWriter` writes a list of `Pixel` objects to file. Each pixel is represented in text on the form "x y color". 
  - A `PixelFileReader` reads a file with pixel data and returns a list of `Pixel` objects.

See also [Where should I store my files](./pixel-files/where-should-i-store-my-files.md), which discusses where to store different types of files.
