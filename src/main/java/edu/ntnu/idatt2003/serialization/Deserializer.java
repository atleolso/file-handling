package edu.ntnu.idatt2003.serialization;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class Deserializer {

    /*
     * Deserialize a serialized object from file.
     */
    public static Object readObjectFromFile(File file) throws IOException {
        Object object = null;
        try (FileInputStream fs = new FileInputStream(file);
             ObjectInputStream is = new ObjectInputStream(fs)) {
            object = is.readObject();
        } catch (ClassNotFoundException e) {
            throw new IOException("Unable to read object from file. " + e.getMessage(), e);
        }
        return object;
    }
}
