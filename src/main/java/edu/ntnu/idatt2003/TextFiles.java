package edu.ntnu.idatt2003;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class TextFiles {

    /*
     * Write Hello World to a file using a buffered writer.
     * The output stream is closed in a finally-block.
     */
    public static void writeToFile(File file) {
        BufferedWriter writer = null;
        try {
            FileWriter fileWriter = new FileWriter(file);
            writer = new BufferedWriter(fileWriter);
            writer.write("Hello\nworld!");
        } catch (IOException e) {
            // handle exception (e.g. log)
        } finally {
            try {
                if (writer != null) {
                   writer.close();
                }
            } catch (IOException e) {
                // handle exception (e.g. log)
            }
        }
    }

    /*
     * Write Hello World to a file with a buffered writer.
     * The try-with-resource statement provides automatic resource management.
     */
    public static void writeToFileTryWithResource(File file) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write("Hello\nworld!");
        } catch (IOException e) {
            // handle exception (e.g. log)
        }
    }

    /*
     * Write Hello World to a file with Java NIO´s Files.writeString(..).
     */
    public static void writeToFileNIO(String filePath) {
        try {
            Files.writeString(Path.of(filePath), "Hello\nworld!", StandardCharsets.UTF_8);
        } catch (IOException e) {
            // handle exception (e.g. log)
        }
    }

    /*
     * Write Hello World to a file with a print writer.
     * We utilize stream chaining for more complex IO operations:
     *   - BufferedWriter: buffers characters for more efficient writing
     *   - PrintWriter: prints formatted representations of objects
     */
    public static void writeToFileStreamChaining(File file) {
        try (FileWriter fileWriter = new FileWriter(file);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
             PrintWriter printWriter = new PrintWriter(bufferedWriter)) {
            printWriter.println("Hello");
            printWriter.println("world!");
        } catch (IOException e) {
            // handle exception (e.g. log)
        }
    }

    /*
     * Read decimal representations of chars from a file with a plain file reader.
     * The decimal value depends on the character encoding.
     */
    public static void readFromFile(File file) {
        try (FileReader fileReader = new FileReader(file)) {
            int c;
            while ((c = fileReader.read()) != -1) {
                // do stuff, e.g. print decimal value and corresponding character in UTF-8
                byte[] b = {(byte)c};
                System.out.println(c + " => " + new String(b, StandardCharsets.UTF_8));
            }
        } catch (IOException e) {
            // handle exception (e.g. log)
        }
    }

    /*
     * Read lines from a file with a buffered reader.
     * Stream chaining enables us to read one line at a time.
     */
    public static void readLinesFromFile(File file) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            // handle exception (e.g. log)
        }
    }

    /*
     * Read lines from a file using Java NIO´s Files.newBufferedReader(..).
     */
    public static void readLinesFromFileNIO(File file) {
        try (BufferedReader bufferedReader = Files.newBufferedReader(file.toPath())) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                // do stuff, e.g. parse line data
            }
        } catch (IOException e) {
            // handle exception (e.g. log)
        }
    }

    /*
     * Read lines from a file using a Scanner.
     */
    public static void readLinesFromFileWithScanner(File file) {
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                System.out.println(line);
            }
        } catch (IOException e) {
            // handle exception (e.g. log)
        }
    }
}
