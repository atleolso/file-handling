package edu.ntnu.idatt2003.pixel;

import edu.ntnu.idatt2003.pixel.io.PixelFileReader;
import edu.ntnu.idatt2003.pixel.io.PixelFileWriter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * App to demonstrate simple writing and reading of text files.
 */
public class PixelApp {

    public static void main(String[] args) {
        Path pixelFilePath = Path.of("./pixel-files/pixels.txt");

        List<Pixel> pixels = new ArrayList<>();
        pixels.add(new Pixel(1, 1, new Color(255, 10, 0)));
        pixels.add(new Pixel(1, 2, new Color(0, 255, 8)));
        pixels.add(new Pixel(2060, 8, new Color(75, 0, 255)));

        try {
            //write pixels to file
            PixelFileWriter pixelFileWriter = new PixelFileWriter();
            pixelFileWriter.writePixels(pixels, pixelFilePath);

            //read pixels from file and print
            PixelFileReader pixelFileReader = new PixelFileReader();
            List<Pixel> readPixels = pixelFileReader.readPixels(pixelFilePath);
            readPixels.forEach(System.out::println);
        } catch (IOException e) {
            System.err.println("App crashed: " + e.getMessage());
        }
    }
}
