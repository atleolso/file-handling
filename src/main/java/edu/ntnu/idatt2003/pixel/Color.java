package edu.ntnu.idatt2003.pixel;

/**
 * A class representing a color in the RGB color space.
 * Every color has three components: red, green and blue, and each component can have a value between 0 and 255.
 */
public class Color {

    private final int red;
    private final int green;
    private final int blue;

    /**
     * Creates a color for a given hex RGB code.
     *
     * @param hexCodeRGB The color as a hex RGB code, e.g. FF0033.
     */
    public Color(String hexCodeRGB) {
        if (!isValidHexCodeRGB(hexCodeRGB)) {
            throw new IllegalArgumentException("'" + hexCodeRGB + "' is not a valid hexadecimal color code");
        }

        // parse the hex code into red, green and blue integer values
        this.red = Integer.parseInt(hexCodeRGB.substring(0, 2), 16);
        this.green = Integer.parseInt(hexCodeRGB.substring(2, 4), 16);
        this.blue = Integer.parseInt(hexCodeRGB.substring(4, 6), 16);
    }


    /**
     * Creates a color with the specified red, green, and blue values in the range (0 - 255).
     *
     * @param red   The red component.
     * @param green The green component.
     * @param blue  The blue component.
     */
    public Color(int red, int green, int blue) {
        if (!isValidDecimalCode(red)) {
            throw new IllegalArgumentException("'" + red + "' is not a valid decimal color code for red");
        }
        if (!isValidDecimalCode(green)) {
            throw new IllegalArgumentException("'" + green + "' is not a valid decimal color code for green");
        }
        if (!isValidDecimalCode(blue)) {
            throw new IllegalArgumentException("'" + blue + "' is not a valid decimal color code for blue");
        }

        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    /**
     * Returns the red component in the range 0-255 in the RGB space.
     *
     * @return The red component.
     */
    public int getRed() {
        return red;
    }

    /**
     * Returns the green component in the range 0-255 in the RGB space.
     *
     * @return The green component.
     */
    public int getGreen() {
        return green;
    }

    /**
     * Returns the blue component in the range 0-255 in the RGB space.
     *
     * @return The blue component.
     */
    public int getBlue() {
        return blue;
    }

    /**
     * Returns the hex code representation of this color.
     * E.g. (3,255,64) would return 03FF40
     *
     * @return The hex code representation of this color.
     */
    public String getHexCodeRGB() {
        String r = red < 16 ? "0" + Integer.toHexString(red) : Integer.toHexString(red);
        String g = green < 16 ? "0" + Integer.toHexString(green) : Integer.toHexString(green);
        String b = blue < 16 ? "0" + Integer.toHexString(blue) : Integer.toHexString(blue);
        return r + g + b;
    }

    @Override
    public String toString() {
        return "Color{" +
                "red=" + red +
                ", green=" + green +
                ", blue=" + blue +
                '}';
    }

    // A valid hex code has 6 characters (valid characters are A-F, a-f, 0-9).
    // Each color is represented by two characters.
    private boolean isValidHexCodeRGB(String hexCodeRGB) {
        final String regex = "[A-Fa-f0-9]{6}";
        return hexCodeRGB.matches(regex);
    }

    // A valid decimal code is an integer >=0 and <= 255.
    private boolean isValidDecimalCode(int decimalCode) {
        return (decimalCode >= 0 && decimalCode <= 255);
    }
}
