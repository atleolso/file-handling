package edu.ntnu.idatt2003.pixel.io;

import edu.ntnu.idatt2003.pixel.Color;
import edu.ntnu.idatt2003.pixel.Pixel;

import java.io.IOException;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * A PixelFileReader reads pixel data from a text file.
 * Each line in the file represents a pixel and must follow the format 'x y color',
 * where x and y are integers and color is a hex color code.
 *
 * <p>
 * Sample file data:
 *
 * <pre>{@code
 * 1 1 ff0000
 * 1 2 00ff00
 * 1 3 0000ff
 * 10 280 cccccc
 * }</pre>
 * </p>
 */
public class PixelFileReader {

    private static final String DELIMITER = " ";

    /**
     * Reads a list of pixels from a text file.
     *
     * @param path A path to a file containing pixel data.
     * @return A list of {@link Pixel} objects.
     * @throws IOException If an I/O error occurs.
     */
    public List<Pixel> readPixels(Path path) throws IOException {
        List<Pixel> pixels = new ArrayList<>();
        try (Scanner scanner = new Scanner(path)) {
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                Pixel pixel = parsePixel(line);
                pixels.add(pixel);
            }
        } catch (Exception e) {
            throw new IOException("Unable to read pixel data from '" + path + "'. " + e.getMessage(), e);
        }
        return pixels;
    }

    private Pixel parsePixel(String line) throws ParseException {
        String[] tokens = line.split(DELIMITER);
        if (tokens.length != 3) {
            throw new ParseException("Line data '" + line + "' is invalid. Make sure each line is on the form 'x y color'.", -1);
        }

        int x;
        int y;
        try {
            x = Integer.parseInt(tokens[0]);
            y = Integer.parseInt(tokens[1]);
        } catch (NumberFormatException e) {
            throw new ParseException("X and Y must be integers (" + e.getMessage() + ")", -1);
        }
        String hexColorRGB = tokens[2];

        Color color = new Color(hexColorRGB);
        return new Pixel(x, y, color);
    }
}
