package edu.ntnu.idatt2003.pixel.io;

import edu.ntnu.idatt2003.pixel.Pixel;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * A PixelFileWriter makes it possible to write {@link Pixel} objects to a file.
 * Each pixel is persisted in the format 'x y color', where x and y are integers and color is a hex color code.
 *
 * <p>
 * Sample file data:
 *
 * <pre>{@code
 * 1 1 ff0000
 * 1 2 00ff00
 * 1 3 0000ff
 * 10 280 cccccc
 * }</pre>
 * </p>
 */
public class PixelFileWriter {

    private static final String DELIMITER = " ";

    /**
     * Writes a list of {@link Pixel} objects to a file.
     *
     * @param pixels A list of {@link Pixel} objects.
     * @param path   A file path.
     * @throws IOException If an I/O error occurs.
     */
    public void writePixels(List<Pixel> pixels, Path path) throws IOException {
        try (BufferedWriter writer = Files.newBufferedWriter(path)) {
            for (Pixel pixel : pixels) {
                String line = pixel.getX() + DELIMITER + pixel.getY() + DELIMITER + pixel.getColor().getHexCodeRGB();
                writer.write(line);
                writer.newLine();
            }
        } catch (IOException e) {
            throw new IOException("Unable to write pixel file. " + e.getMessage(), e);
        }
    }
}
