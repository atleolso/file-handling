package edu.ntnu.idatt2003.pixel;

/**
 * A class for a pixel.
 * A pixel represents a location in (x,y) coordinate space, specified in integer precision, and has a color.
 */
public class Pixel {

    private final int x;
    private final int y;
    private final Color color;

    /**
     * Creates a new pixel with a specified (x,y) location and color.
     *
     * @param x     The X coordinate.
     * @param y     The Y coordinate.
     * @param color The color.
     */
    public Pixel(int x, int y, Color color) {
        if (color == null) {
            throw new IllegalArgumentException("A color cannot be null");
        }
        this.x = x;
        this.y = y;
        this.color = color;
    }

    /**
     * Returns the X coordinate of this pixel.
     *
     * @return The X coordinate of this pixel.
     */
    public int getX() {
        return x;
    }

    /**
     * Returns the Y coordinate of this pixel.
     *
     * @return The Y coordinate of this pixel.
     */
    public int getY() {
        return y;
    }

    /**
     * Returns the color of this pixel.
     *
     * @return The color of this pixel.
     */
    public Color getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "Pixel{" +
                "x=" + x +
                ", y=" + y +
                ", color=" + color +
                '}';
    }
}
